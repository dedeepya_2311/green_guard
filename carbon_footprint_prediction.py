import pandas as pd
from sklearn.preprocessing import LabelEncoder,StandardScaler
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
import pickle as pk


df = pd.read_csv("C:/Users/aald5/OneDrive/Documents/carbon_emissions.csv")
le = LabelEncoder()
cat_col = [col for col in df.columns if df[col].dtype == 'object']
# apply label encoding to the categorical data
for cat in cat_col:
    df[cat] = le.fit_transform(df[cat])
num_col = [col for col in df.columns if df[col].dtype != 'object']
scaler = StandardScaler()
df[num_col] = scaler.fit_transform(df[num_col])
X = df.drop('CarbonEmission', axis=1)  # Features (all columns except 'CarbonEmission')
y = df['CarbonEmission']
correlation_matrix = df.corr()
target_correlation = correlation_matrix['CarbonEmission']
plt.figure(figsize=(12, 8))
sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm')
plt.show()
relevant_features = target_correlation[target_correlation > 0.01]
x_train,x_test,y_train,y_test = train_test_split(X,y,test_size = 0.2,random_state=42)
model4 = RandomForestRegressor()
model4.fit(x_train,y_train)
y_pred = model4.predict(x_test)


pk.dump(model4,open('model.pkl','rb'))
model = pk.load(open('model.pkl','rb'))